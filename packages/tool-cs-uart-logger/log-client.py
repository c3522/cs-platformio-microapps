#!/usr/bin/env python3

import subprocess
from pathlib import Path
import os
import sys
import time

# We have to call a sub process because PlatformIO keeps the output in a buffer, and this buffer 
# will couse delays in the logs. We use the -u option in python as well to to get unbuffered 
# stdout

source_path = Path(__file__).resolve()
source_dir = source_path.parent

command = ["python", "-u", os.path.join(source_dir, 'log-client-script.py')]


try:

    # We also need a way to close the logger since it's running as a sepparate process now. A
    # solution for this is to run the script forever and have the process terminate with a 
    # KeyboardInterrupt

    p = subprocess.Popen(command)

    while(1):
        pass

except KeyboardInterrupt:
    p.terminate()
