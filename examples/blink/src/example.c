#include <Arduino.h>

void setup() {
	Serial.begin();
	if (!Serial) return;
}

int loop() {

	Serial.println("Hello Crownstone");

	digitalWrite(CS_MICROAPP_COMMAND_PIN_SWITCH, 0);
	delay(500);
	digitalWrite(CS_MICROAPP_COMMAND_PIN_SWITCH, 1);
	delay(500);

	return 1;
}
