# PlatformIO Crownstone Microapp Integration

This repository contains the integration for the Crownstone Microapps into PlatformIO.
This integration is based on the [crownstone-microapp](https://github.com/crownstone/crownstone-microapp) project.

## Setup

To setup a PlatformIO project simply run the following commands:

	mkdir my-microapp-project
	cd my-microapp-project
	pio init

Now we need to edit the PlatformIO configuration file, *platformio.ini*.
 You can use one of the files in the **examples** directory.
For more documentation on how to create a PlatformIO configuration file
check out [PlatformIO Project Configuration](https://docs.platformio.org/en/latest/projectconf/index.html).
Add the following lines to the *platformio.ini* file.
This is the basic configuration we need to create Microapps.

```
[env:microapp]
platform = https://gitlab.com/c3522/cs-platformio/platform-csmicroapps
framework = arduino
board = crownstone
```

## Compiling

New we are ready to compile a Microapp. Pick one of the examples from **examples** or the code bellow, and run the command:

	pio run

src/example.c

```c
// Blink!
#include <Arduino.h>

void setup() {
	Serial.begin();
	if (!Serial) return;
}

int loop() {

	Serial.println("Hello Crownstone");

	digitalWrite(CS_MICROAPP_COMMAND_PIN_SWITCH, 0);
	delay(500);
	digitalWrite(CS_MICROAPP_COMMAND_PIN_SWITCH, 1);
	delay(500);

	return 1;
}
```

This command should start to download the appropriate platform and tools for the project, after which it will compile the code.
## Uploading

We also want the option to upload Microapps to the Crownstone or development board.
We can either do this via UART or BLE.


	[env:microapp]
	platform = https://gitlab.com/c3522/cs-platformio/platform-csmicroapps
	framework = arduino
	board = crownstone

	#Uncomment the following line to upload wired
	#upload_protocol = wired

	#Uncomment the following lines to upload via UART
	#upload_protocol = ble
	#upload_flags =
	#	--bleAddress FD:FC:93:F8:97:BA
	#	--adapterAddress 00:1A:7D:DA:71:13
	#	--spherefile sphere.json

For the BLE option we also need to set a few flags:

* **--bleAddress** is the Bluetooth address of the Crownstone you are trying to connect too.
* **--adapterAddress** is the address of your computers' internal Bluetooth adapter.
* **--spherefile** is a flag that requires a Json file to Crownstone sphere keys. The Json file should look like the following:

```json
{
 "admin":  "adminKeyForCrown",
 "member": "memberKeyForHome",
 "basic":  "basicKeyForOther",
 "serviceDataKey":  "MyServiceDataKey",
 "localizationKey":  "aLocalizationKey",
 "meshApplicationKey":  "MyGoodMeshAppKey",
 "meshNetworkKey":  "MyGoodMeshNetKey",
}
```

## Monitoring

Since Bluenet doesn't send plain text via UART, we can't use the internal PlatformIO monitor.
We will have to use a target to get logs from the Crownstones. We can do this with the following command:

	pio run -t log

## Targets

To list the environment targets run the following command:

	pio run --list-targets

This should output the following text with descriptions what these targets can do.
To run a target simply run the command: *pio run -t \<target_name>*.

```
Environment    Group     Name       Title                 Description
-------------  --------  ---------  --------------------  ------------------------------------------------------------
microapp       Advanced  compiledb  Compilation Database  Generate compilation database `compile_commands.json`
microapp       General   clean      Clean
microapp       General   cleanall   Clean All             Clean a build environment and installed library dependencies
microapp       Platform  log        Log                   Monitor the Crownstone via UART
microapp       Platform  upload     Upload                Upload a microapp via BLE or UART to a Crownstone
```
